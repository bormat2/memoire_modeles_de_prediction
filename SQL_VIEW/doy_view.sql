CREATE OR REPLACE VIEW public.doy_view AS 
 SELECT _date._date,
    a._doy AS doy
   FROM generate_series('2012-01-01 00:00:00'::timestamp without time zone, '2018-11-11 00:00:00'::timestamp without time zone, '1 day'::interval) _date(_date)
     JOIN LATERAL ( SELECT date_part('year'::text, _date._date)::integer AS date_part,
            date_part('doy'::text, _date._date) AS date_part) y(iyear, doy) ON true
     JOIN LATERAL ( SELECT (y.iyear % 4) = 0 AND ((y.iyear % 100) <> 0 OR (y.iyear % 400) = 0)) z(is_bisex) ON true
     JOIN LATERAL ( SELECT
                CASE
                    WHEN z.is_bisex AND y.doy > 59::double precision THEN
                    CASE
                        WHEN y.doy = 60::double precision THEN 59.5::double precision
                        ELSE y.doy - 1::double precision
                    END
                    ELSE y.doy
                END AS _doy) a ON true;

ALTER TABLE public.doy_view
  OWNER TO postgres;
