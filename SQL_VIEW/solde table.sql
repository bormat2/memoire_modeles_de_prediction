-- View: public.data3

DROP VIEW if exists public.data3;
drop table if exists solde2;
CREATE TABLE solde2(d_start date, d_end date, period TEXT);
INSERT INTO solde2 
    SELECT *
    FROM (values 
        ('2012-01-11'::date,  '2012-02-14'::date , 'SOLD_HIVER'),
        ('2013-01-09'::date, '2013-02-12'::date , 'SOLD_HIVER'),
        ('2014-01-08'::date, '2014-02-11'::date , 'SOLD_HIVER'),
        ('2015-01-07'::date, '2015-02-17'::date , 'SOLD_HIVER'),
        ('2016-01-06'::date, '2016-02-16'::date , 'SOLD_HIVER'),
        ('2017-01-11'::date, '2017-02-21'::date , 'SOLD_HIVER'),
        ('2018-01-10'::date, '2018-02-20'::date , 'SOLD_HIVER'),
        ('2012-07-04'::date, '2012-08-07'::date, 'SOLD_ETE'),
        ('2013-06-26'::date, '2013-07-30'::date, 'SOLD_ETE'),
        ('2014-06-25'::date, '2014-07-29'::date, 'SOLD_ETE'),
        ('2015-06-24'::date, '2015-08-04'::date, 'SOLD_ETE'),
        ('2016-06-22'::date, '2016-08-02'::date, 'SOLD_ETE'),
        ('2017-06-28'::date, '2017-08-08'::date, 'SOLD_ETE'),
        ('2018-06-27'::date, '2018-08-07'::date, 'SOLD_ETE'),
        ('2012-03-21'::date, '2012-04-01'::date, 'PROMO_PRINTEMPS'),
        ('2013-03-13'::date, '2013-04-01'::date, 'PROMO_PRINTEMPS'),
        ('2015-03-18'::date, '2015-03-30'::date, 'PROMO_PRINTEMPS'),
        ('2016-03-16'::date, '2016-03-28'::date, 'PROMO_PRINTEMPS'),
        ('2014-03-12'::date, '2014-03-23'::date, 'PROMO_PRINTEMPS'),
        ('2017-03-11'::date, '2017-03-26'::date, 'PROMO_PRINTEMPS'),
        ('2018-03-21'::date, '2018-04-10'::date, 'PROMO_PRINTEMPS'),
        ('2012-09-03'::date, '2012-10-14'::date, 'PROMP_AUTOMNE'),
        ('2013-10-02'::date, '2013-10-13'::date, 'PROMP_AUTOMNE'),
        ('2014-10-01'::date, '2014-10-12'::date, 'PROMP_AUTOMNE'),
        ('2015-10-07'::date, '2015-10-17'::date, 'PROMP_AUTOMNE'),
        ('2016-10-12'::date, '2016-10-22'::date, 'PROMP_AUTOMNE'),
        ('2017-10-11'::date, '2017-11-01'::date, 'PROMP_AUTOMNE'),
        ('2012-01-01'::date , '2012-01-01'::date, 'FERIE'),
        ('2012-01-06'::date , '2012-01-06'::date, 'FERIE'),
        ('2012-02-14'::date , '2012-02-14'::date, 'FERIE'),
        ('2012-02-21'::date , '2012-02-21'::date, 'FERIE'),
        ('2012-04-08'::date , '2012-04-08'::date, 'FERIE'),
        ('2012-04-08'::date , '2012-04-08'::date, 'FERIE'),
        ('2012-04-09'::date , '2012-04-09'::date, 'FERIE'),
        ('2012-05-01'::date , '2012-05-01'::date, 'FERIE'),
        ('2012-05-08'::date , '2012-05-08'::date, 'FERIE'),
        ('2012-05-17'::date , '2012-05-17'::date, 'FERIE'),
        ('2012-05-27'::date , '2012-05-27'::date, 'FERIE'),
        ('2012-05-27'::date , '2012-05-27'::date, 'FERIE'),
        ('2012-05-28'::date , '2012-05-28'::date, 'FERIE'),
        ('2012-06-14'::date , '2012-06-14'::date, 'FERIE'),
        ('2012-09-15'::date , '2012-09-15'::date, 'FERIE'),
        ('2012-11-01'::date , '2012-11-01'::date, 'FERIE'),
        ('2012-11-11'::date , '2012-11-11'::date, 'FERIE'),
        ('2012-12-25'::date , '2012-12-25'::date, 'FERIE'),
        ('2012-12-31'::date , '2012-12-31'::date, 'FERIE'),
        ('2013-01-01'::date , '2013-01-01'::date, 'FERIE'),
        ('2013-01-06'::date , '2013-01-06'::date, 'FERIE'),
        ('2013-02-12'::date , '2013-02-12'::date, 'FERIE'),
        ('2013-02-14'::date , '2013-02-14'::date, 'FERIE'),
        ('2013-03-31'::date , '2013-03-31'::date, 'FERIE'),
        ('2013-03-31'::date , '2013-03-31'::date, 'FERIE'),
        ('2013-04-01'::date , '2013-04-01'::date, 'FERIE'),
        ('2013-05-01'::date , '2013-05-01'::date, 'FERIE'),
        ('2013-05-08'::date , '2013-05-08'::date, 'FERIE'),
        ('2013-05-09'::date , '2013-05-09'::date, 'FERIE'),
        ('2013-05-19'::date , '2013-05-19'::date, 'FERIE'),
        ('2013-05-19'::date , '2013-05-19'::date, 'FERIE'),
        ('2013-05-20'::date , '2013-05-20'::date, 'FERIE'),
        ('2013-06-14'::date , '2013-06-14'::date, 'FERIE'),
        ('2013-09-15'::date , '2013-09-15'::date, 'FERIE'),
        ('2013-11-01'::date , '2013-11-01'::date, 'FERIE'),
        ('2013-11-11'::date , '2013-11-11'::date, 'FERIE'),
        ('2013-12-25'::date , '2013-12-25'::date, 'FERIE'),
        ('2013-12-31'::date , '2013-12-31'::date, 'FERIE'),
        ('2014-01-01'::date , '2014-01-01'::date, 'FERIE'),
        ('2014-01-06'::date , '2014-01-06'::date, 'FERIE'),
        ('2014-02-14'::date , '2014-02-14'::date, 'FERIE'),
        ('2014-03-04'::date , '2014-03-04'::date, 'FERIE'),
        ('2014-04-20'::date , '2014-04-20'::date, 'FERIE'),
        ('2014-04-20'::date , '2014-04-20'::date, 'FERIE'),
        ('2014-04-21'::date , '2014-04-21'::date, 'FERIE'),
        ('2014-05-01'::date , '2014-05-01'::date, 'FERIE'),
        ('2014-05-08'::date , '2014-05-08'::date, 'FERIE'),
        ('2014-05-29'::date , '2014-05-29'::date, 'FERIE'),
        ('2014-06-08'::date , '2014-06-08'::date, 'FERIE'),
        ('2014-06-08'::date , '2014-06-08'::date, 'FERIE'),
        ('2014-06-09'::date , '2014-06-09'::date, 'FERIE'),
        ('2014-06-14'::date , '2014-06-14'::date, 'FERIE'),
        ('2014-09-15'::date , '2014-09-15'::date, 'FERIE'),
        ('2014-11-01'::date , '2014-11-01'::date, 'FERIE'),
        ('2014-11-11'::date , '2014-11-11'::date, 'FERIE'),
        ('2014-12-25'::date , '2014-12-25'::date, 'FERIE'),
        ('2014-12-31'::date , '2014-12-31'::date, 'FERIE'),
        ('2015-01-01'::date , '2015-01-01'::date, 'FERIE'),
        ('2015-01-06'::date , '2015-01-06'::date, 'FERIE'),
        ('2015-02-14'::date , '2015-02-14'::date, 'FERIE'),
        ('2015-02-17'::date , '2015-02-17'::date, 'FERIE'),
        ('2015-04-05'::date , '2015-04-05'::date, 'FERIE'),
        ('2015-04-06'::date , '2015-04-06'::date, 'FERIE'),
        ('2015-05-01'::date , '2015-05-01'::date, 'FERIE'),
        ('2015-05-08'::date , '2015-05-08'::date, 'FERIE'),
        ('2015-05-14'::date , '2015-05-14'::date, 'FERIE'),
        ('2015-05-24'::date , '2015-05-24'::date, 'FERIE'),
        ('2015-05-24'::date , '2015-05-24'::date, 'FERIE'),
        ('2015-05-25'::date , '2015-05-25'::date, 'FERIE'),
        ('2015-06-14'::date , '2015-06-14'::date, 'FERIE'),
        ('2015-09-15'::date , '2015-09-15'::date, 'FERIE'),
        ('2015-11-01'::date , '2015-11-01'::date, 'FERIE'),
        ('2015-11-11'::date , '2015-11-11'::date, 'FERIE'),
        ('2015-12-25'::date , '2015-12-25'::date, 'FERIE'),
        ('2015-12-31'::date , '2015-12-31'::date, 'FERIE'),
        ('2016-01-01'::date , '2016-01-01'::date, 'FERIE'),
        ('2016-01-06'::date , '2016-01-06'::date, 'FERIE'),
        ('2016-02-09'::date , '2016-02-09'::date, 'FERIE'),
        ('2016-02-14'::date , '2016-02-14'::date, 'FERIE'),
        ('2016-03-27'::date , '2016-03-27'::date, 'FERIE'),
        ('2016-03-27'::date , '2016-03-27'::date, 'FERIE'),
        ('2016-03-28'::date , '2016-03-28'::date, 'FERIE'),
        ('2016-05-01'::date , '2016-05-01'::date, 'FERIE'),
        ('2016-05-05'::date , '2016-05-05'::date, 'FERIE'),
        ('2016-05-08'::date , '2016-05-08'::date, 'FERIE'),
        ('2016-05-15'::date , '2016-05-15'::date, 'FERIE'),
        ('2016-05-15'::date , '2016-05-15'::date, 'FERIE'),
        ('2016-05-16'::date , '2016-05-16'::date, 'FERIE'),
        ('2016-06-14'::date , '2016-06-14'::date, 'FERIE'),
        ('2016-09-15'::date , '2016-09-15'::date, 'FERIE'),
        ('2016-11-01'::date , '2016-11-01'::date, 'FERIE'),
        ('2016-11-11'::date , '2016-11-11'::date, 'FERIE'),
        ('2016-12-25'::date , '2016-12-25'::date, 'FERIE'),
        ('2016-12-31'::date , '2016-12-31'::date, 'FERIE'),
        ('2017-01-01'::date , '2017-01-01'::date, 'FERIE'),
        ('2017-01-06'::date , '2017-01-06'::date, 'FERIE'),
        ('2017-02-14'::date , '2017-02-14'::date, 'FERIE'),
        ('2017-02-28'::date , '2017-02-28'::date, 'FERIE'),
        ('2017-04-16'::date , '2017-04-16'::date, 'FERIE'),
        ('2017-04-16'::date , '2017-04-16'::date, 'FERIE'),
        ('2017-04-17'::date , '2017-04-17'::date, 'FERIE'),
        ('2017-05-01'::date , '2017-05-01'::date, 'FERIE'),
        ('2017-05-08'::date , '2017-05-08'::date, 'FERIE'),
        ('2017-05-25'::date , '2017-05-25'::date, 'FERIE'),
        ('2017-06-04'::date , '2017-06-04'::date, 'FERIE'),
        ('2017-06-04'::date , '2017-06-04'::date, 'FERIE'),
        ('2017-06-05'::date , '2017-06-05'::date, 'FERIE'),
        ('2017-06-14'::date , '2017-06-14'::date, 'FERIE'),
        ('2017-09-15'::date , '2017-09-15'::date, 'FERIE'),
        ('2017-11-01'::date , '2017-11-01'::date, 'FERIE'),
        ('2017-11-11'::date , '2017-11-11'::date, 'FERIE'),
        ('2017-12-25'::date , '2017-12-25'::date, 'FERIE'),
        ('2017-12-31'::date , '2017-12-31'::date, 'FERIE'),
        ('2018-01-01'::date , '2018-01-01'::date, 'FERIE'),
        ('2018-01-06'::date , '2018-01-06'::date, 'FERIE'),
        ('2018-02-13'::date , '2018-02-13'::date, 'FERIE'),
        ('2018-02-14'::date , '2018-02-14'::date, 'FERIE'),
        ('2018-04-01'::date , '2018-04-01'::date, 'FERIE'),
        ('2018-04-01'::date , '2018-04-01'::date, 'FERIE'),
        ('2018-04-02'::date , '2018-04-02'::date, 'FERIE'),
        ('2018-05-01'::date , '2018-05-01'::date, 'FERIE'),
        ('2018-05-08'::date , '2018-05-08'::date, 'FERIE'),
        ('2018-05-10'::date , '2018-05-10'::date, 'FERIE'),
        ('2018-05-20'::date , '2018-05-20'::date, 'FERIE'),
        ('2018-05-20'::date , '2018-05-20'::date, 'FERIE'),
        ('2018-05-21'::date , '2018-05-21'::date, 'FERIE'),
        ('2018-06-14'::date , '2018-06-14'::date, 'FERIE'),
        ('2018-09-15'::date , '2018-09-15'::date, 'FERIE'),
        ('2018-11-01'::date , '2018-11-01'::date, 'FERIE'),
        ('2018-11-11'::date , '2018-11-11'::date, 'FERIE'),
        ('2018-12-25'::date , '2018-12-25'::date, 'FERIE'),
        ('2018-12-31'::date , '2018-12-31'::date, 'FERIE')
     ) x 
;


CREATE OR REPLACE TABLE eq_per AS 
SELECT *
FROM (values 
('2015-01-01','2015-01-01'),
('2015-01-03','2015-01-02'),
('2015-01-04','2016-01-03'),
('2015-01-05','2016-01-04'),
('2015-01-07','2016-01-06'),
('2015-01-08','2016-01-07'),
('2015-01-09','2016-01-08'),
('2015-01-10','2016-01-09'),
('2015-01-11','2016-01-10'),
('2015-01-12','2016-01-11'),
('2015-01-13','2016-01-12'),
('2015-01-14','2016-01-13'),
('2015-01-15','2016-01-14'),
('2015-01-16','2016-01-15'),
('2015-01-17','2016-01-16'),
('2015-01-18','2016-01-17'),
('2015-01-19','2016-01-18'),
('2015-01-20','2016-01-19'),
('2015-01-21','2016-01-20'),
('2015-01-22','2016-01-21'),
('2015-01-23','2016-01-22'),
('2015-01-24','2016-01-23'),
('2015-01-25','2016-01-24'),
('2015-01-26','2016-01-25'),
('2015-01-27','2016-01-26'),
('2015-01-28','2016-01-27'),
('2015-01-29','2016-01-28'),
('2015-01-30','2016-01-29'),
('2015-01-31','2016-01-30'),
('2015-02-01','2016-01-31'),
('2015-02-02','2016-02-01'),
('2015-02-03','2016-02-02'),
('2015-02-04','2016-02-03'),
('2015-02-05','2016-02-04'),
('2015-02-06','2016-02-05'),
('2015-02-07','2016-02-06'),
('2015-02-08','2016-02-07'),
('2015-02-09','2016-02-08'),
('2015-02-11','2016-02-10'),
('2015-02-12','2016-02-11'),
('2015-02-13','2016-02-12'),
('2015-02-14','2016-02-14'),
('2015-02-16','2016-02-15'),
('2015-02-17','2016-02-09'),
('2015-02-18','2016-02-17'),
('2015-02-19','2016-02-18'),
('2015-02-20','2016-02-19'),
('2015-02-21','2016-02-20'),
('2015-02-22','2016-02-21'),
('2015-02-23','2016-02-22'),
('2015-02-24','2016-02-23'),
('2015-02-25','2016-02-24'),
('2015-02-26','2016-02-25'),
('2015-02-27','2016-02-26'),
('2015-02-28','2016-02-27'),
('2015-03-01','2016-02-28'),
('2015-03-02','2016-02-29'),
('2015-03-03','2016-03-01'),
('2015-03-04','2016-03-02'),
('2015-03-05','2016-03-03'),
('2015-03-06','2016-03-04'),
('2015-03-07','2016-03-05'),
('2015-03-08','2016-03-06'),
('2015-03-09','2016-03-07'),
('2015-03-10','2016-03-08'),
('2015-03-11','2016-03-09'),
('2015-03-12','2016-03-10'),
('2015-03-13','2016-03-11'),
('2015-03-14','2016-03-12'),
('2015-03-15','2016-03-13'),
('2015-03-16','2016-03-14'),
('2015-03-17','2016-03-15'),
('2015-03-18','2016-03-16'),
('2015-03-19','2016-03-17'),
('2015-03-20','2016-03-18'),
('2015-03-21','2016-03-19'),
('2015-03-22','2016-03-20'),
('2015-03-23','2016-03-21'),
('2015-03-24','2016-03-22'),
('2015-03-25','2016-03-23'),
('2015-03-26','2016-03-24'),
('2015-03-27','2016-03-25'),
('2015-03-28','2016-03-26'),
('2015-03-31','2016-03-29'),
('2015-04-01','2016-03-30'),
('2015-04-02','2016-03-31'),
('2015-04-03','2016-04-01'),
('2015-04-04','2016-04-02'),
('2015-04-05','2016-03-27'),
('2015-04-06','2016-03-28'),
('2015-04-07','2016-04-05'),
('2015-04-08','2016-04-06'),
('2015-04-09','2016-04-07'),
('2015-04-10','2016-04-08'),
('2015-04-11','2016-04-09'),
('2015-04-12','2016-04-10'),
('2015-04-13','2016-04-11'),
('2015-04-14','2016-04-12'),
('2015-04-15','2016-04-13'),
('2015-04-16','2016-04-14'),
('2015-04-17','2016-04-15'),
('2015-04-18','2016-04-16'),
('2015-04-19','2016-04-17'),
('2015-04-20','2016-04-18'),
('2015-04-21','2016-04-19'),
('2015-04-22','2016-04-20'),
('2015-04-23','2016-04-21'),
('2015-04-24','2016-04-22'),
('2015-04-25','2016-04-23'),
('2015-04-26','2016-04-24'),
('2015-04-27','2016-04-25'),
('2015-04-28','2016-04-26'),
('2015-04-29','2016-04-27'),
('2015-04-30','2016-04-28'),
('2015-05-01','2016-05-01'),
('2015-05-02','2016-04-30'),
('2015-05-04','2016-05-02'),
('2015-05-05','2016-05-03'),
('2015-05-06','2016-05-04'),
('2015-05-08','2016-05-08'),
('2015-05-09','2016-05-07'),
('2015-05-11','2016-05-09'),
('2015-05-12','2016-05-10'),
('2015-05-13','2016-05-11'),
('2015-05-14','2016-05-05'),
('2015-05-15','2016-05-13'),
('2015-05-16','2016-05-14'),
('2015-05-19','2016-05-17'),
('2015-05-20','2016-05-18'),
('2015-05-21','2016-05-19'),
('2015-05-22','2016-05-20'),
('2015-05-23','2016-05-21'),
('2015-05-24','2016-05-15'),
('2015-05-25','2016-05-16'),
('2015-05-26','2016-05-24'),
('2015-05-27','2016-05-25'),
('2015-05-28','2016-05-26'),
('2015-05-29','2016-05-27'),
('2015-05-30','2016-05-28'),
('2015-05-31','2016-05-29'),
('2015-06-01','2016-05-30'),
('2015-06-02','2016-05-31'),
('2015-06-03','2016-06-01'),
('2015-06-04','2016-06-02'),
('2015-06-05','2016-06-03'),
('2015-06-06','2016-06-04'),
('2015-06-07','2016-06-05'),
('2015-06-08','2016-06-06'),
('2015-06-09','2016-06-07'),
('2015-06-10','2016-06-08'),
('2015-06-11','2016-06-09'),
('2015-06-12','2016-06-10'),
('2015-06-13','2016-06-11'),
('2015-06-14','2016-06-14'),
('2015-06-15','2016-06-13'),
('2015-06-17','2016-06-15'),
('2015-06-18','2016-06-16'),
('2015-06-19','2016-06-17'),
('2015-06-20','2016-06-18'),
('2015-06-21','2016-06-19'),
('2015-06-22','2016-06-20'),
('2015-06-23','2016-06-21'),
('2015-06-24','2016-06-22'),
('2015-06-25','2016-06-23'),
('2015-06-26','2016-06-24'),
('2015-06-27','2016-06-25'),
('2015-06-28','2016-06-26'),
('2015-06-29','2016-06-27'),
('2015-06-30','2016-06-28'),
('2015-07-01','2016-06-29'),
('2015-07-02','2016-06-30'),
('2015-07-03','2016-07-01'),
('2015-07-04','2016-07-02'),
('2015-07-05','2016-07-03'),
('2015-07-06','2016-07-04'),
('2015-07-07','2016-07-05'),
('2015-07-08','2016-07-06'),
('2015-07-09','2016-07-07'),
('2015-07-10','2016-07-08'),
('2015-07-11','2016-07-09'),
('2015-07-12','2016-07-10'),
('2015-07-13','2016-07-11'),
('2015-07-14','2016-07-12'),
('2015-07-15','2016-07-13'),
('2015-07-16','2016-07-14'),
('2015-07-17','2016-07-15'),
('2015-07-18','2016-07-16'),
('2015-07-19','2016-07-17'),
('2015-07-20','2016-07-18'),
('2015-07-21','2016-07-19'),
('2015-07-22','2016-07-20'),
('2015-07-23','2016-07-21'),
('2015-07-24','2016-07-22'),
('2015-07-25','2016-07-23'),
('2015-07-26','2016-07-24'),
('2015-07-27','2016-07-25'),
('2015-07-28','2016-07-26'),
('2015-07-29','2016-07-27'),
('2015-07-30','2016-07-28'),
('2015-07-31','2016-07-29'),
('2015-08-01','2016-07-30'),
('2015-08-02','2016-07-31'),
('2015-08-03','2016-08-01'),
('2015-08-04','2016-08-02'),
('2015-08-05','2016-08-03'),
('2015-08-06','2016-08-04'),
('2015-08-07','2016-08-05'),
('2015-08-08','2016-08-06'),
('2015-08-09','2016-08-07'),
('2015-08-10','2016-08-08'),
('2015-08-11','2016-08-09'),
('2015-08-12','2016-08-10'),
('2015-08-13','2016-08-11'),
('2015-08-14','2016-08-12'),
('2015-08-15','2016-08-13'),
('2015-08-16','2016-08-14'),
('2015-08-17','2016-08-15'),
('2015-08-18','2016-08-16'),
('2015-08-19','2016-08-17'),
('2015-08-20','2016-08-18'),
('2015-08-21','2016-08-19'),
('2015-08-22','2016-08-20'),
('2015-08-23','2016-08-21'),
('2015-08-24','2016-08-22'),
('2015-08-25','2016-08-23'),
('2015-08-26','2016-08-24'),
('2015-08-27','2016-08-25'),
('2015-08-28','2016-08-26'),
('2015-08-29','2016-08-27'),
('2015-08-30','2016-08-28'),
('2015-08-31','2016-08-29'),
('2015-09-01','2016-08-30'),
('2015-09-02','2016-08-31'),
('2015-09-03','2016-09-01'),
('2015-09-04','2016-09-02'),
('2015-09-05','2016-09-03'),
('2015-09-06','2016-09-04'),
('2015-09-07','2016-09-05'),
('2015-09-08','2016-09-06'),
('2015-09-09','2016-09-09'),
('2015-09-10','2016-09-08'),
('2015-09-12','2016-09-10'),
('2015-09-13','2016-09-11'),
('2015-09-14','2016-09-12'),
('2015-09-15','2016-09-13'),
('2015-09-16','2016-09-14'),
('2015-09-17','2016-09-15'),
('2015-09-18','2016-09-16'),
('2015-09-19','2016-09-17'),
('2015-09-20','2016-09-18'),
('2015-09-21','2016-09-19'),
('2015-09-22','2016-09-20'),
('2015-09-23','2016-09-21'),
('2015-09-24','2016-09-22'),
('2015-09-25','2016-09-23'),
('2015-09-26','2016-09-24'),
('2015-09-27','2016-09-25'),
('2015-09-28','2016-09-26'),
('2015-09-29','2016-09-27'),
('2015-09-30','2016-09-28'),
('2015-10-01','2016-09-29'),
('2015-10-02','2016-09-30'),
('2015-10-03','2016-10-01'),
('2015-10-04','2016-10-02'),
('2015-10-05','2016-10-03'),
('2015-10-06','2016-10-04'),
('2015-10-07','2016-10-12'),
('2015-10-08','2016-10-13'),
('2015-10-09','2016-10-14'),
('2015-10-10','2016-10-15'),
('2015-10-11','2016-10-16'),
('2015-10-12','2016-10-17'),
('2015-10-13','2016-10-18'),
('2015-10-14','2016-10-19'),
('2015-10-15','2016-10-20'),
('2015-10-16','2016-10-21'),
('2015-10-17','2016-10-22'),
('2015-10-25','2016-10-23'),
('2015-10-26','2016-10-24'),
('2015-10-27','2016-10-25'),
('2015-10-28','2016-10-26'),
('2015-10-29','2016-10-27'),
('2015-10-30','2016-10-28'),
('2015-10-31','2016-10-29'),
('2015-11-01','2016-11-01'),
('2015-11-02','2016-10-31'),
('2015-11-04','2016-11-02'),
('2015-11-05','2016-11-03'),
('2015-11-06','2016-11-04'),
('2015-11-07','2016-11-05'),
('2015-11-08','2016-11-06'),
('2015-11-09','2016-11-07'),
('2015-11-10','2016-11-08'),
('2015-11-11','2016-11-11'),
('2015-11-12','2016-11-10'),
('2015-11-14','2016-11-12'),
('2015-11-15','2016-11-13'),
('2015-11-16','2016-11-14'),
('2015-11-17','2016-11-15'),
('2015-11-18','2016-11-16'),
('2015-11-19','2016-11-17'),
('2015-11-20','2016-11-18'),
('2015-11-21','2016-11-19'),
('2015-11-22','2016-11-20'),
('2015-11-23','2016-11-21'),
('2015-11-24','2016-11-22'),
('2015-11-25','2016-11-23'),
('2015-11-26','2016-11-24'),
('2015-11-27','2016-11-25'),
('2015-11-28','2016-11-26'),
('2015-11-29','2016-11-27'),
('2015-11-30','2016-11-28'),
('2015-12-01','2016-11-29'),
('2015-12-02','2016-11-30'),
('2015-12-03','2016-12-01'),
('2015-12-04','2016-12-02'),
('2015-12-05','2016-12-03'),
('2015-12-06','2016-12-04'),
('2015-12-07','2016-12-05'),
('2015-12-08','2016-12-06'),
('2015-12-09','2016-12-07'),
('2015-12-10','2016-12-08'),
('2015-12-11','2016-12-09'),
('2015-12-12','2016-12-10'),
('2015-12-13','2016-12-11'),
('2015-12-14','2016-12-12'),
('2015-12-15','2016-12-13'),
('2015-12-16','2016-12-14'),
('2015-12-17','2016-12-15'),
('2015-12-18','2016-12-16'),
('2015-12-19','2016-12-17'),
('2015-12-20','2016-12-18'),
('2015-12-21','2016-12-19'),
('2015-12-22','2016-12-20'),
('2015-12-23','2016-12-21'),
('2015-12-24','2016-12-22'),
('2015-12-25','2016-12-25'),
('2015-12-26','2016-12-24'),
('2015-12-28','2016-12-26'),
('2015-12-29','2016-12-27'),
('2015-12-30','2016-12-28'),
('2015-12-31','2016-12-29'),
('2015-12-25','2016-12-25'),
('2015-12-31','2016-12-31')
) x (st_old,end_new);
-- View: public.data3

-- DROP VIEW public.data3;

CREATE OR REPLACE VIEW public.data3 AS 
 SELECT data2.debit,
    date_part('month'::text, data2.idi_jour) AS _month,
    to_char(data2.idi_jour::timestamp without time zone, 'YYYYMMDD'::text)::integer AS idi_jour,
    sum(data2.debit) OVER (PARTITION BY data2.idi_jour) AS total_day,
    data2.debit::numeric / sum(data2.debit) OVER (PARTITION BY data2.idi_jour) AS repartition_day,
    data2.debit::numeric / avg(data2.debit) OVER (PARTITION BY data2.idi_jour) AS ratio_debit_by_mean,
    date_part('year'::text, data2.idi_jour) AS year,
    date_part('day'::text, data2.idi_jour) AS pos_in_month,
    data2.ftime,
    data2.dow,
    0 AS avg_deb,
    data2.week,
    (data2.dow = 0)::integer AS dow_0,
    (data2.dow = 1)::integer AS dow_1,
    (data2.dow = 2)::integer AS dow_2,
    (data2.dow = 3)::integer AS dow_3,
    (data2.dow = 4)::integer AS dow_4,
    (data2.dow = 5)::integer AS dow_5,
    (data2.dow = 6)::integer AS dow_6,
    data2.temp,
    data2.cloud_prct,
    data2.rain,
    data2.snow,
    data2.temp_min,
    data2.temp_max,
    data2.wind_deg,
    data2.wind_speed,
    data2.humidity,
    doy5.doy,
    solde.solde_hiver,
    solde.solde_ete,
    solde.ferie,
    solde.solde_hiver + solde.solde_ete AS solde_ete_ou_hiver,
    solde.position_prom_solde,
    solde.printemps_prom,
    solde.autumn_prom,
    solde.printemps_prom + solde.autumn_prom AS prom,
    d2015.debit as debit_2015
FROM data2
left JOIN eq_per ON eq_per.end_new = data.idi_jour
left JOIN data2 d2015 ON eq_per.st_old = d2015.idi_jour AND d2015.ftime = data2.ftime
JOIN doy_view doy5 ON  doy5._date = data2.idi_jour 
JOIN (
    SELECT
        idi_jour,ftime,
        SUM( (COALESCE(solde2.period, ''::text) = 'SOLD_HIVER'::text)::integer ) AS solde_hiver,
        SUM( (COALESCE(solde2.period, ''::text) = 'SOLD_ETE'::text)::integer ) AS solde_ete,
        SUM( (COALESCE(solde2.period, ''::text) = 'PROMO_PRINTEMPS'::text)::integer ) AS printemps_prom,
        SUM( (COALESCE(solde2.period, ''::text) = 'PROMO_AUTOMNE'::text)::integer ) AS autumn_prom,
        SUM( (COALESCE(solde2.period, ''::text) = 'FERIE'::text)::integer ) AS ferie,
        SUM(CASE
            WHEN solde2.period IS NOT NULL AND solde2.period <> 'FERIE'::text THEN data2.idi_jour - solde2.d_start + 1
            ELSE 0
        END) AS position_prom_solde
    FROM data2
    LEFT JOIN solde2 ON data2.idi_jour >= solde2.d_start AND data2.idi_jour <= solde2.d_end
    GROUP BY idi_jour,ftime
) solde ON data2.idi_jour = solde.idi_jour AND data2.ftime = solde.ftime;



Copy (Select * From data3) To '/var/www/html/Mem/Memoire/forecast/data/dataoutput.csv' With HEADER CSV DELIMITER ',';
