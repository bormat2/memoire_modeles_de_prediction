-- View: public.data2

-- DROP VIEW public.data2;

CREATE OR REPLACE VIEW public.data2 AS 
 SELECT sum(bo.debit) AS debit,
    split_part(bo.tranche_horaire::text, ':'::text, 1)::integer::double precision + split_part(bo.tranche_horaire::text, ':'::text, 2)::double precision / 60::double precision AS ftime,
    date_part('week'::text, bo.idi_jour)::integer AS week,
    date_part('dow'::text, bo.idi_jour)::integer AS dow,
    date_part('doy'::text, bo.idi_jour)::integer AS doy,
    m.cloud_prct,
        CASE
            WHEN m.rain IS NOT NULL THEN m.rain
            ELSE 0::double precision
        END AS rain,
        CASE
            WHEN m.snow IS NOT NULL THEN m.snow
            ELSE 0::double precision
        END AS snow,
    m.temp,
    m.temps_min AS temp_min,
    m.temp_max,
    m.wind_deg,
    m.wind_speed,
    m.humidity,
    m.pressure
   FROM borto bo
     JOIN ( SELECT "météo".hour_fr AS end_hour,
            "météo".hour_fr - '01:00:00'::interval AS start_hour,
            "météo".date_fr,
            "météo".hour_fr,
            "météo"._date,
            "météo"._hour,
            "météo".city_id,
            "météo".cloud_prct,
            "météo".humidity,
            "météo".pressure,
            "météo".rain,
            "météo".snow,
            "météo".temp,
            "météo".temps_min,
            "météo".temp_max,
            "météo".wind_deg,
            "météo".wind_speed
           FROM "météo") m ON m.date_fr = bo.idi_jour AND bo.tranche_horaire::time without time zone > m.start_hour AND bo.tranche_horaire::time without time zone <= m.end_hour
  WHERE bo.lbl_site::text = 'GL METZ'::text
  GROUP BY (split_part(bo.tranche_horaire::text, ':'::text, 1)::integer::double precision + split_part(bo.tranche_horaire::text, ':'::text, 2)::double precision / 60::double precision), (date_part('week'::text, bo.idi_jour)::integer), (date_part('dow'::text, bo.idi_jour)::integer), (date_part('doy'::text, bo.idi_jour)::integer), m.cloud_prct, m.humidity, m.pressure, m.rain, m.snow, m.temp, m.temps_min, m.temp_max, m.wind_deg, m.wind_speed
  ORDER BY (date_part('doy'::text, bo.idi_jour)::integer);

ALTER TABLE public.data2
  OWNER TO postgres;



