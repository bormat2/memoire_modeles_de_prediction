import numpy as np;
import pandas as pd;
from sklearn.model_selection import train_test_split;
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from collections import defaultdict
from sklearn import neighbors
from sklearn import tree
from sklearn.ensemble import RandomForestRegressor
from sklearn.decomposition import PCA
from sklearn import preprocessing
import warnings
from math import sqrt
import random


#import estimators# import DecisionTreeRegressor
#from tree_builders import TreeType

class Reference:
    def __init__(self,var1):
        self.value = var1
        
def customKnnDist(a87,b87):
    dfa = pd.DataFrame(a87.reshape(-1,len(x_train_ref.value.columns.values)),columns=x_train_ref.value.columns.values);
    dfb = pd.DataFrame(b87.reshape(-1,len(x_train_ref.value.columns.values)),columns=x_train_ref.value.columns.values);
    return np.sum(np.abs(dfa['doy'] - dfb['doy']))
    
    
    
    
separator = '__________' #assez long pour que le trie par ordre alphabetique marche bien
FAKE_DATA = False
WEEK_ONLY = False
WEEK_END_ONLY = False
RESTRICT_HOUR = True
NORMALIZE_DATAv1 = False
DO_NOT_USE_METEO = False
REMOVE_JANUARY = False
REMOVE_DOY_END = False;
REMOVE_SOLDE_START = False;
ADD_COLUMNS_MOST_ERROR = False
SEPARATE_2017 = False
LASSO=False;#peut être trés lent si alpha petit et trés mauvais si alpha grand
ALPHA=0.001;#seuelement si lasso = true, si 0 alors mert lasso à false
USE_SIGMOIDE = True
TEST_SIZE_PRCT= 0.1
THE_YEAR = 2016
MIN_YEAR = 2012
MAX_YEAR = 2019
SOLDE_MODE = 0; # 1 pour que solde et promo, 2 pour pas solde et pas promo et 0 pour les 2
WEIGHT = 'distance'
partitions = ['train','test']#train doit être au tout début
methods = [#'regress','mean','knn','mean_dow_ftime',
           'mean',
          'tree',
          'tree3',
#          'forest','regress',
#          'knn','mean'
          ]
METRIC = 'manhattan';#['euclidean','mahalanobis','wminkowski','minkowski','chebyshev','manhattan','seuclidean'];
ATTENTAT_IGNORED = True; #supprimer la période de solde qui a été totalement perturbé par les attentats
INCREASE_SOLDE_IMPORTANCE = False;
SEPARATE_DAY_AND_RATIO = False #on fait un modèle pour la moyenne sur la journée puis un modèle sur le rapport avec la moyenne et on combine les 2 après
# on va faire la moyenne par ftime avec ou non le jour calculé sur la partie d'entrainement 
# et l'injecter dans les donneées de test
USE_MEAN_ON_KEYS = False;
USE_MEAN_ON_KEYS_DOW_FTIME = True; #moyenne par jour de la semaine et heure la colonne sera mean_dow_ftime
USE_MEAN_ON_KEY_ONLY = False;
EXCLURE_DOY_NOT_ENOUGHT_DATA = True;
GROUP_BY_DAY = False;
NB_NEIGHBORS = 7;
YEAR_BY_YEAR = True;
OPTIMISE_REG_LIN = True
#COLUMNS_TO_REMOVE = set([
#        'year','idi_jour','ftime','_month','dow','week',
#        'temp','rain'
#        ,'snow','wind_deg','doy','mean_dow_ftime','forest'
#        
#        ])
        #,'dow','week',
#                     'solde_hiver','solde_ete','solde_ete_ou_hiver'
DUMMIES_COLUMNS2 = [
#       {'col_name': 'rain','nb_cluster': 2},
#        {'col_name': 'temp','nb_cluster': 7},
#        {'col_name': 'cloud_prct', 'nb_cluster':3},
#        {'col_name': 'week', 'nb_cluster':52},
#        {'col_name': 'doy', 'nb_cluster':False},
#       {'col_name': 'ftime', 'nb_cluster': False},
#        {'col_name': 'pos_in_month', 'nb_cluster':31},
#        {'col_name': 'doy_modif_v2', 'nb_cluster':365},
#        {'col_name': 'dow_0t', 'nb_cluster':52},
#        {'col_name': 'dow_1t', 'nb_cluster':52},
#        {'col_name': 'dow_2t', 'nb_cluster':52},
#        {'col_name': 'dow_3t', 'nb_cluster':52},
#        {'col_name': 'dow_4t', 'nb_cluster':52},
#        {'col_name': 'dow_5t', 'nb_cluster':52},
#        {'col_name': 'dow_6t', 'nb_cluster':52},
#        {'col_name': 'doy_modif_v2', 'nb_cluster':52},

];#dummies = une valeur devient une colonne
COLUMNS_TO_REMOVE = set([
        'avg_deb',
        'v1',
        'v2',
        'v3',
        'v4',
        'v5',
        'v6',
        'v7',
        'v8',
        'v9',
        'v10',
        'year',
        'idi_jour',
        'saturday_solde_promo_pos',
        'ferie_solde',
        'promo_solde',
#        'solde_ete_ou_hiver',
        '_month',
#        'dow',
                   'wind_speed',
           'humidity',
        'dow_0',
        'dow_1',
        'dow_2',
        'dow_3',
        'dow_4',
        'dow_5',
        'dow_6',
        'dow6',
        'week',
        'temp',
        'snow',
        'wind_deg',
#        'doy',
        'mean_dow_ftime'
        ,'one_for_cst','total_day','repartition_day',
        'cloud_prct',
        'rain',
        'temp_min',
        'temp_max',
        'solde_ete',
        'solde_hiver',
        'cloud_prct',
        'pos_in_month',
        'mean_dow_ftime',
        'debit_2015',
        'prom'
        ])

if OPTIMISE_REG_LIN:
#    COLUMNS_TO_REMOVE = (COLUMNS_TO_REMOVE - set(['mean_dow_ftime'])) | set(['ftime','dow'])
    DUMMIES_COLUMNS2 = [
#       {'col_name': 'rain','nb_cluster': 2},
#        {'col_name': 'temp','nb_cluster': 7},
#        {'col_name': 'cloud_prct', 'nb_cluster':3},
#        {'col_name': 'week', 'nb_cluster':52},
#        {'col_name': 'doy', 'nb_cluster':False},
#       {'col_name': 'ftime', 'nb_cluster': False},
#        {'col_name': 'pos_in_month', 'nb_cluster':31},
#        {'col_name': 'doy_modif_v2', 'nb_cluster':365},
#        {'col_name': 'dow_0t', 'nb_cluster':52},
#        {'col_name': 'dow_1t', 'nb_cluster':52},
#        {'col_name': 'dow_2t', 'nb_cluster':52},
#        {'col_name': 'dow_3t', 'nb_cluster':52},
#        {'col_name': 'dow_4t', 'nb_cluster':52},
#        {'col_name': 'dow_5t', 'nb_cluster':52},
#        {'col_name': 'dow_6t', 'nb_cluster':52},
#        {'col_name': 'doy_modif_v2', 'nb_cluster':52},
        
];#dummies = une valeur devient une colonne
#avg_deb doit être présent pour avoir une bonne regression linéaire
# il est parcontre contre productif pour l'arbre    
COLUMN_TO_KEEP_ONLY = set([
#                        'ftime',# très important
#                        'solde_ete_ou_hiver', #si je sépare le test 2017 est mieux mais les autres moins biens
#                        'debit',
#                        'position_prom_solde', #très utile avec le mode SEPARATE_DAY_AND_RATIO
#                        'autumn_prom',
#                        'printemps_prom',
#                        'ferie',#pas très utile
##                        'dow6',# un peu mais pas tant que ça
#                        'doy',#inutilisé
#                        'dow', #bof étrangement
#                        'saturday_solde_promo_pos',
##                        'promo_solde',
#                        'ratio_debit_by_mean',
#                        
##                         'position_prom_solde_error',
##            'doy_error_193',
##            'doy_error_269',
##            'doy_start_error'
# #  'solde_ete_ou_hiver',#,'avg_deb'#,'mean_dow_ftime'
## 'prom',
# #'week','_month',
##  'rain',
#  
##  'solde_hiver','solde_ete'
]);              
#COLUMN_TO_KEEP_ONLY = set(['debit','doy','ftime','dow','ferie',
#   'solde_ete_ou_hiver'#,'avg_deb'#,'mean_dow_ftime'
#  ,'prom'
#  ,'ratio_debit_by_mean'
#]);
                       

CSV_PATH = '/var/www/html/Mem/Memoire/forecast/data/dataoutput.csv'
#from collections import defaultdict;
np.set_printoptions(suppress=True)
print('toto');
def values_to_columns(column_name,x_train_ref,x_tests_ref,nb_inter):
    df_train = x_train_ref.value[column_name]
    if nb_inter == False:
        pseudo_bin = np.sort(x_train_ref.value[column_name].unique())
        obj_train = {}
        for key in pseudo_bin:
            obj_train[column_name + '_' + str(key)] = df_train == key
        x_train_ref.value = x_train_ref.value.join(pd.DataFrame(obj_train))

        for dtest in x_tests_ref:
            obj_test= {};
            for key in pseudo_bin:
                obj_test[column_name + '_' + str(key)] = dtest.value[column_name] == key
            # dummies_tests.append(pd.DataFrame(obj_test))
            dtest.value = dtest.value.join(pd.DataFrame(obj_test))
    else:
        df_train,bins = pd.cut(df_train,nb_inter,retbins=True)
        dummies_temp_train = pd.get_dummies(df_train,prefix=column_name)      
        #on ajoute une colonne par intervale de température
        dummies_temp_train.columns = map(str,dummies_temp_train.columns)
        
        for dtest in x_tests_ref:
            df_test = dtest.value[column_name];
            df_test = pd.cut(df_test,bins,retbins=False)
            dummies_temp_test = pd.get_dummies(df_test,prefix=column_name)
            dummies_temp_test.columns = map(str,dummies_temp_test.columns);    
            # dummies_tests.append(dummies_temp_test)
            dtest.value = dtest.value.join(dummies_temp_test)
    
def change_column_order(df, col_name, index):
    cols = df.columns.tolist()
    cols.remove(col_name)
    cols.insert(index, col_name)
    return df[cols]
#!/usr/bin/python

# Function definition is here
# def fill_mean_by_time2(mean_by_time_sum,mean_by_time_count,mean_by_time):
#     keys = mean_by_time_sum.keys();
#     values = mean_by_time_sum.values();
#     range_dico = range(0,len(keys));
#     for k, ftime2 in enumerate(list(mean_by_time_sum.values())):
#         mean_by_time[ftime2] =  mean_by_time_sum[ftime2] / mean_by_time_count[ftime2];
if FAKE_DATA:
    data =   pd.concat([pd.DataFrame({
        "debit": [5,15,10,12.5,17.5,17.5,12.5,10,7.5,2.5],
        "ftime": [8,9,10,11,12,13,14,15,16,17],
        "doy": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "dow": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "temp": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "pos_in_month": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "year": [2016,2016,2016,2016,2016,2016,2016,2016,2016,2016],
        "avg_deb": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "solde_hiver": [1,1,0,0,0,0,0,0,0,0]
    }), pd.DataFrame({
        "debit": [2.5,15,20,12.5,17.5,17.5,12.5,10,7.5,2.5],
        "ftime": [8,9,10,11,12,13,14,15,16,17],
        "doy": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "dow": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "temp": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "pos_in_month": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "year": [2014,2014,2014,2014,2014,2014,2014,2014,2014,2014],
        "avg_deb": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "solde_hiver": [0,1,1,0,0,0,0,0,0,0],
    }), pd.DataFrame({
        "debit": [2.5,7.5,10,12.5,17.5,17.5,12.5,20,7.5,2.5],
        "ftime": [8,9,10,11,12,13,14,15,16,17],
        "doy": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "dow": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "temp": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "pos_in_month": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "year": [2017,2017,2017,2017,2017,2017,2017,2017,2017,2017],
        "avg_deb": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "solde_hiver": [0,0,0,0,0,0,0,1,0,0]
    }),pd.DataFrame({
        "debit": [2.5,7.5,10,12.5,17.5,17.5,25,20,7.5,2.5],
        "ftime": [8,9,10,11,12,13,14,15,16,17],
        "doy": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "dow": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "temp": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "pos_in_month": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "year": [2015,2015,2015,2015,2015,2015,2015,2015,2015,2017],
        "avg_deb": [1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0],
        "solde_hiver": [0,0,0,0,0,0,1,1,0,0]
    })]);
    data = change_column_order(data,'debit',0)
else:
    data = pd.read_csv(CSV_PATH);
data = data.join(pd.DataFrame({
    'promo_solde': data['solde_ete_ou_hiver'] + data['prom'],
    'saturday_solde_promo_pos' : (data['dow'] == 6) * (data['position_prom_solde']) ,
    'dow6': data['dow'] == 6
} ));

tot_error = 0; tot_mean = 0;
data8 = data;
for i_grp in [1]: #,2,3,4,5,6,8,9,10,12,13,15,16,20,60]:
    data = data8[data8['cod_grp'] == i_grp];
    data = data.drop(columns=['cod_grp'])
    # peu importe férié ce sont les débuts des solde
    #data.ferie =  (data['ferie'] > 0) & ((data['promo_solde']  < 1) | (data['position_prom_solde'] > 14));
    
    # peu importe doy si ce sont les soldes
    #data.doy = data['doy'] * (data['promo_solde'] < 1);
    
    
    data_origin = data.copy(deep=True);
    
    if RESTRICT_HOUR:
        data = data[(data['ftime'] >1) & (data['ftime'] < 22)];
    if ADD_COLUMNS_MOST_ERROR:
        data = data.join(pd.DataFrame({
                'position_prom_solde_error': (data['position_prom_solde'] > 0) & (data['position_prom_solde'] < 14.5) ,
                'doy_error_193': data['doy'] < 193,
                'doy_error_269': data['doy'] < 269,      
                'doy_start_error': data['doy'] < 28
    }))
    if GROUP_BY_DAY:
        USE_MEAN_ON_KEYS = False;
        
        data44 = data.groupby(['idi_jour']).agg({
        	'debit': 'mean',
            'saturday_solde_promo_pos': 'mean',
            'promo_solde': 'mean',
        	'temp': 'mean',
        	'cloud_prct': 'mean',
        	'rain': 'mean',
        	'snow': 'mean',
        	'temp_min': 'mean',
        	'temp_max' :'mean',
        	'wind_deg': 'mean',
        	'wind_speed': 'mean',
        	'humidity': 'mean',
    #         'position_prom_solde_error':'mean',
    #            'doy_error_193':'mean',
    #            'doy_error_269':'mean',
    #            'doy_start_error':'mean',
    #    
        	#group by
            'ratio_debit_by_mean':'mean',
            'promo_solde' : 'mean',
        	'idi_jour': 'mean',
            'printemps_prom': 'mean',
            'autumn_prom': 'mean',
        	'doy': 'mean',
        	'_month': 'mean',
        	'solde_hiver': 'mean',
        	'solde_ete': 'mean',
        	'ferie': 'mean',
        	'solde_ete_ou_hiver': 'mean',
        	'position_prom_solde': 'mean',
        	'prom': 'mean',
        	'year': 'mean',
        	'pos_in_month': 'mean',
        	'dow': 'mean',
        	'week': 'mean',
        	'dow': 'mean'
        })
        data44.index = range(0,len(data44));
        data44 = data44.join(pd.DataFrame({'ftime': np.zeros(len(data44))}));
        data = change_column_order(data44,'debit',0)
       
    if USE_MEAN_ON_KEYS:
        the_key_debit = data_origin[['debit']].join(pd.DataFrame({  
                'the_key':  #data.one_for_cstunic_key
                    data_origin.ftime.map(str) +'-'+ data_origin.doy.map(str),
                'the_key_dowft':  #data.one_for_cstunic_key
                    data_origin.ftime.map(str) +'-'+ data_origin.dow.map(str)
        }));
        unic_key = the_key_debit.the_key.unique()
        unic_key_dow_ft = the_key_debit.the_key_dowft.unique()
        #par défault on met les débits à 0
        avg_deb_default = pd.DataFrame({'the_key': unic_key,
                                       'debit': np.zeros(len(unic_key))});
        avg_deb_default_dow_ftime = pd.DataFrame({'the_key': unic_key_dow_ft,
                                       'debit': np.zeros(len(unic_key_dow_ft))});
    
    
    #del data['temp_max'];
    #del data['temp_min'];
    #del data['temp'];
    #del data['week'];
    #del data['dow_0'];
    #del data['dow_2'];
    #del data['dow_3'];
    #del data['dow_4'];
    #del data['dow_5'];
    #del data['dow_6'];
    
    
    if EXCLURE_DOY_NOT_ENOUGHT_DATA:
        data = data[(data['idi_jour'] != 20131002)]
    if ATTENTAT_IGNORED:
        data = data[(data['idi_jour'] > 20150107) | (data['idi_jour'] < 20150107)]
    if SOLDE_MODE:
        if SOLDE_MODE == 1 :
            data = data[data['promo_solde'] == 1]
        else:
            data = data[data['promo_solde'] == 0]
        data.index = range(len(data))
    list_data_columns = list(data.columns)
    i_d_debit = list_data_columns.index('debit')
    
    if DO_NOT_USE_METEO:
        data.drop(columns=['temp','temp_min','temp_max','snow','rain','wind_speed','wind_deg','humidity'],inplace=True);
    if WEEK_ONLY:
        data = data[data['dow'] < 6];
    if WEEK_END_ONLY:
        data = data[data['dow'] > 5];
    if MIN_YEAR:
        data = data[(data['year'] >= MIN_YEAR) & (data['year'] <= MAX_YEAR)]
    
    #data=data.join(pd.DataFrame({'index1':list(data.index)}));
    
    if REMOVE_JANUARY:
        data = data[data['_month'] > 1]
    if REMOVE_DOY_END:
        data = data[data['doy'] < 193] #269
    if REMOVE_SOLDE_START:
        data = data[(data['position_prom_solde'] < 0) | (data['position_prom_solde'] > 14.5)]
        
    nb_row =len(data);
    data = data.join(
            pd.DataFrame({
                'ferie_solde' : (data['ferie'] > 0) & (data['promo_solde'] > 0),
                 # for constante add column of 1 
                'one_for_cst': np.ones(nb_row),
                'mean_dow_ftime':  np.ones(nb_row),#sera remplit plus tard c'est la moyenne par jour et par heure
    
            }) 
    )
    ########################################################################
        #16.86 18.90
      #@coef
    co1 = 20
    if not OPTIMISE_REG_LIN:
        data['ftime'] = data['ftime'] * 1500 * co1   # sinon 45
        #data['v2_vac'] = data['vac2'] * 1500 * co1
        data['dow'] = data['dow'] * 1150 * co1  #sinon 23
        data['position_prom_solde'] = data['position_prom_solde'] * 1000 * co1 #sinon 19.30
        data['ferie'] = data['ferie'] * 1000 * co1  #sinon 19.30
        data['printemps_prom'] = data['printemps_prom'] * 700 * co1  # sinon 18.90
        data['autumn_prom'] = data['autumn_prom'] * 700 * co1 #sinon 18.90
        data['solde_ete_ou_hiver'] = data['solde_ete_ou_hiver'] * 700 * co1 #sinon 18.90
        #data['rain'] = data['rain'] * 20000 #sinon 18.8977543209 et 1686
    data = data * 1;
    #data['debit'] = np.sqrt(np.sqrt(np.sqrt(data['debit'])))
    if SEPARATE_2017:
        data_2017 = data[data['year'] == THE_YEAR]
        data = data[data['year'] != THE_YEAR]
        data_2017['doy'] = data_2017['doy'] * 1000 * co1 #sinon 35
    
        y_2017 = data_2017.iloc[:,i_d_debit]
        x_2017 = data_2017.iloc[:,i_d_debit + 1:]
        data_2017.index = range(0,len(data_2017));#on ne veut pas de trou dans nos indices
    
    data['doy'] = data['doy'] * 1000 * co1                         #sinon 35
        #data['temp'] = data['temp'] * 2000 # sinon 1679 et 1899
        #data['solde_ete_ou_hiver'] = data['solde_ete_ou_hiver
    #####################################################################
    
    
    
    if NORMALIZE_DATAv1:
    #    data = pd.DataFrame(preprocessing.scale(data * 100),columns=data.columns)#- data.min())
    #    data_2017 = pd.DataFrame(preprocessing.scale(data_2017 * 100),columns=data_2017.columns)#- data.min())
        data = data * 0.5;
        y_2017 = y_2017 * 0.5;
        x_2017 = x_2017 * 0.5;
        
    
    data.index = range(0,len(data));#on ne veut pas de trou dans nos indices
    y = data.iloc[:,i_d_debit]
    x = data.iloc[:,i_d_debit + 1:]
    
    #i_d_ftime = list_data_columns.index('ftime') # n'est pas dans x ne sert pas à la regression
    nb_loop = 2
    #e = {
    #    "test": 0,
    #    "train": 0,
    #    "mean_tot": 0,
    #    "mean_custom" : 0
    #}
    i = 1;
    pos_e_mean_doyftime = 0;
    #frame d'association clef débit
    #on crée une clef sur le temp pour faire la moyenne dessus
    
    # C'EST ICI QUE L4ON CHOISIT LA CLEF DE LA MOYENNE PERSONNALISÉ
    
    
    e = defaultdict(float)
    if USE_MEAN_ON_KEYS: methods.append('mean_custom')
    if SEPARATE_2017: partitions.append('2017')
    
    list_x_columns = list(x.columns)
    set_x_columns = set(list_x_columns)
    if len(COLUMN_TO_KEEP_ONLY):
        COLUMNS_TO_REMOVE = set_x_columns - COLUMN_TO_KEEP_ONLY
      
    
    #data_day = days.agg({
    #    	'debit': 'mean',
    #        'saturday_solde_promo_pos': 'mean',
    #        'promo_solde': 'mean',
    #    	'temp': 'mean',
    #    	'cloud_prct': 'mean',
    #    	'rain': 'mean',
    #    	'snow': 'mean',
    #    	'temp_min': 'mean',
    #    	'temp_max' :'mean',
    #    	'wind_deg': 'mean',
    #    	'wind_speed': 'mean',
    #    	'humidity': 'mean',
    #    
    #    	#group by
    #        'promo_solde' : 'mean',
    #    	'idi_jour': 'mean',
    #        'printemps_prom': 'mean',
    #        'autumn_prom': 'mean',
    #    	'doy': 'mean',
    #    	'_month': 'mean',
    #    	'solde_hiver': 'mean',
    #    	'solde_ete': 'mean',
    #    	'ferie': 'mean',
    #    	'solde_ete_ou_hiver': 'mean',
    #    	'position_prom_solde': 'mean',
    #    	'prom': 'mean',
    #    	'year': 'mean',
    #    	'pos_in_month': 'mean',
    #    	'dow': 'mean',
    #    	'week': 'mean',
    #    	'dow': 'mean'
    #})
    #data_day.index = range(0,len(data_day));
    #data_day = data_day.join(pd.DataFrame({'ftime': np.zeros(len(data_day))}));
    #data = change_column_order(data_day,'debit',0)
        
      
        
    # ON sépare les journée que l'on va testé et pas tester
    key4 = 'idi_jour';
    if YEAR_BY_YEAR:
        key4 = 'year';
        nb_loop = 5;
    days = data.groupby(by=[key4])
    days_key = list(days.groups.keys());
    #fournir une valeur pour avoir le même mélange à chaque fois et ainsi pouvoir comparer
    random.seed(4);#fournir une valeur pour avoir le même mélange à chaque fois et ainsi pouvoir comparer
    random.shuffle(days_key);
    test_size = int(TEST_SIZE_PRCT * len(days_key))
    data_split = []
    print('beforeloop')
    if YEAR_BY_YEAR:
        test_size = 2;
    for i in range(0,nb_loop):
        print('i',i)
        dates_test = days_key[i:i+test_size]
        print(days_key[i:i+test_size])
    #    dates_train = list(set(days_key) - set(dates_test))
        
        group_arr =  [True,False] if SEPARATE_DAY_AND_RATIO else [False]
        for is_group_by_day in group_arr:       
            x_train2 = x[~x[key4].isin(dates_test)];
            y_train2 = y[x_train2.index];
            x_test2 = x[x[key4].isin(dates_test)];
            y_test2 = y[x_test2.index];
        
            to_app = {
                'x_2017': Reference(x_2017.copy(deep=True)) if SEPARATE_2017 else None,
                'y_2017': Reference(y_2017.copy(deep=True)) if SEPARATE_2017 else None,
                'x_train': Reference(x_train2),
                'y_train': Reference(y_train2),
                'x_test': Reference(x_test2),
                'y_test': Reference(y_test2),
                'is_group_by_day' : is_group_by_day
        	}
            data_split.append(to_app)
            obj2 = {
                '2017': {
                    'x': to_app['x_2017'],
                    'y': to_app['y_2017']
                },
                'train': {
                    'x': to_app['x_train'],
                    'y': to_app['y_train']
                },
                'test':{
                    'x': to_app['x_test'],
                    'y': to_app['y_test']  
                }
            }
            to_app['obj2'] = obj2;
            for x_name in partitions:
                #Quand on group par jour il faut enlever le ratio_debit_by_mean
                if is_group_by_day:
                        dat = obj2[x_name]['x'].value.join(obj2[x_name]['y'].value)
                        dat = dat.groupby(['idi_jour'],as_index=False).mean()
                        dat.index = range(len(dat));
                        obj2[x_name]['y'].value = dat['debit']
                        obj2[x_name]['x'].value = dat[list(set(dat.columns.values) - set(['debit']))]
                        obj2[x_name]['x'].value.drop(columns=['ratio_debit_by_mean','ftime'], inplace=True) #supprimer ce ratio qui n'a pas de sens grouper en jour
    
                else:
                    # quand on est pas groupé en jour on doit deviner le ratio par rapport à 
                    # la moyenne de la journée
                    if SEPARATE_DAY_AND_RATIO:
                        obj2[x_name]['y_debit'] = Reference(obj2[x_name]['y'].value)#backup des débits par heure
                        obj2[x_name]['y'].value = obj2[x_name]['x'].value.ratio_debit_by_mean#nouveau y est le ratio par rapport à la moyenne
                    obj2[x_name]['x'].value.drop(columns='ratio_debit_by_mean', inplace=True) #supprimer ce ratio de x vu qu'on doit le deviner
            
        
        
    #x_2017 = None
    #y_2017 = None
    total_lg = 0;
    for i in range(0,nb_loop * (1 + SEPARATE_DAY_AND_RATIO)):
        print('i2',i)
    
        x_test_ref = data_split[i]['x_test']
        x_train_ref = data_split[i]['x_train']
        
        y_test_ref = data_split[i]['y_test']
        y_train_ref = data_split[i]['y_train']
        
        is_group_by_day =  data_split[i]['is_group_by_day']
       # x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=TEST_SIZE_PRCT, random_state=i);
    
    
    #    y_rep =  Reference(data['ratio_debit_by_mean'].iloc[x_train_ref.value.index]);     #ratio à appliquer sur la moyenne de la journée pour avoir le débit sur cette heure
    #    y_rep_test =  Reference(data['ratio_debit_by_mean'].iloc[x_test_ref.value.index]);     #ratio à appliquer sur la moyenne de la journée pour avoir le débit sur cette heure
        set_of_data = [x_train_ref,x_test_ref]#à la fois entrainement et test
        if INCREASE_SOLDE_IMPORTANCE:
            x_train_ref.value = (x_train_ref.value.T * (1 + (x_train_ref.value['solde_ete_ou_hiver']==1)*0.5 )).T #on multiple par 2 les ligne de solde
        x_tests_ref = [x_test_ref]
        if SEPARATE_2017:
    #        x_2017_bis = x_2017.copy(deep=True)
    #        x_2017_ref = Reference(x_2017_bis)
            x_2017_ref = data_split[i]['x_2017']
            y_2017_ref =  data_split[i]['y_2017']
    #        y_rep_2017 = data['ratio_debit_by_mean'].iloc[x_train_ref.value.index];     #ratio à appliquer sur la moyenne de la journée pour avoir le débit sur cette heure
            x_tests_ref.append(x_2017_ref)
            set_of_data.append(x_2017_ref)
            
        mean = np.mean(y_train_ref.value);
        # i_index = x_train_ref[0].index
        if USE_MEAN_ON_KEYS:
            train_the_key_debit = the_key_debit.iloc[x_train_ref.value.index]
            avg_deb_by_the_key = train_the_key_debit.groupby(['the_key'],as_index = False).mean() # see describe too
            
            mean_dow_ftime_by_key = train_the_key_debit.groupby(['the_key_dowft'],as_index = False).mean() # see describe too
            # pour ne pas avoir de problème on met de 0 pour les ftime qui ne sont pas dans l'entrainement avec concat
            # moyenne par the_key
            avg_deb_by_the_key = pd.concat([avg_deb_default,avg_deb_by_the_key],sort=True)\
                .groupby('the_key').sum()
            mean_dow_ftime2 = pd.concat([mean_dow_ftime_by_key,avg_deb_default_dow_ftime],sort=True).groupby('the_key').sum()
            
    
            for setd in set_of_data:
                asso = the_key_debit.iloc[setd.value.index]
                
                defVal = pd.DataFrame({'the_key': asso.the_key,'debit': np.zeros(len(asso.the_key))})
                setd.value.avg_deb = avg_deb_by_the_key.loc[asso.the_key].debit.values
                setd.value.mean_dow_ftime = mean_dow_ftime2.loc[asso.the_key_dowft].debit.values
            
        for setd in set_of_data:
            setd.value.index = range(len(setd.value.index));
    
        if len(DUMMIES_COLUMNS2):
            for obj_2 in list(DUMMIES_COLUMNS2):
                #ajout des valeurs des colonnes en colonnes booléenne
                values_to_columns(column_name=obj_2['col_name'],x_train_ref=x_train_ref,x_tests_ref=x_tests_ref,nb_inter=obj_2['nb_cluster']);       
        
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            for setd in set_of_data:
               for col_name in COLUMNS_TO_REMOVE:
                    if col_name in setd.value.columns.values:
                        col = setd.value[col_name]
                        columns = setd.value.drop(columns=col_name,inplace=True);
                        exec('setd.value.'+col_name+'= col')#hidden column
                
    #    for i in ['group_by_day','repartition']
        if is_group_by_day:
            oldresult99 = {'train':{},'test':{},'2017':{}}
        print('i3',i)
        for partition in partitions:
            _x ,_y = {
                'train': (x_train_ref.value,y_train_ref.value),
                'test' : (x_test_ref.value,y_test_ref.value),
                '2017' : (x_2017_ref.value,y_2017_ref.value) if SEPARATE_2017 else None,
            }[partition]
            if SEPARATE_DAY_AND_RATIO:
                if is_group_by_day:
                     _y =  data_split[i]['obj2'][partition]['y'].value
                else:
                    _y =  data_split[i]['obj2'][partition]['y_debit'].value
            _x.fillna(_x.mean(), inplace=True)
            _y.index = range(0,len(_y))
            for method in methods:
                key7 =  method  + separator + partition 
                if method == 'tree':
                    if partition == 'train':
                        tree2 = DecisionTreeRegressor() 
                        tree2.fit(x_train_ref.value, y_train_ref.value)
                    result99 = tree2.predict(_x)
                elif method == 'tree3':
                    if partition == 'train':
                        #min_samples_leaf=5,min_impurity_decrease=0.1
    #                    tree3 = DecisionTreeRegressor(splitter='best',max_depth=15,max_leaf_nodes=200) #min_samples_split=0,min_impurity_decrease=0.3
                        tree3 = DecisionTreeRegressor(criterion="mse",splitter='best',max_depth=15,min_samples_leaf=2,) #min_samples_split=0,min_impurity_decrease=0.3
                        tree3.fit(x_train_ref.value, y_train_ref.value);
                    result99 =  tree3.predict(_x)
                elif method == 'forest': #with pca
                    if partition == 'train':
                        pca = PCA(n_components=5)
                        train_features = pca.fit_transform(x_train_ref.value * 1)
                        rfr = RandomForestRegressor(n_estimators = 2, n_jobs = 1,random_state = 2016, verbose = 1,oob_score=True)
                        rfr.fit(train_features,y_train_ref.value)
                    test_features = pca.transform(_x * 1)
                    result99 = rfr.predict(test_features)
    #                    tree6 = RandomForestRegressor(n_estimators=10,criterion="mse",max_depth=15,min_samples_leaf=2) #min_samples_split=0,min_impurity_decrease=0.3
    #                    tree6.fit(x_train_ref.value, y_train_ref.value);
    #                    result99 =  tree6.predict(_x)
                elif method == 'tree5':
                    if partition == 'train':
                        #min_samples_leaf=5,min_impurity_decrease=0.1
    #                    tree3 = DecisionTreeRegressor(splitter='best',max_depth=15,max_leaf_nodes=200) #min_samples_split=0,min_impurity_decrease=0.3
                        tree5 = estimators.DecisionTreeRegressor(criterion="mse",splitter='best',min_samples_leaf=2,) #min_samples_split=0,min_impurity_decrease=0.3
                        tree5.fit(x_train_ref.value, y_train_ref.value);
                    result99 =  tree5.predict(_x)
                elif method == 'regress' :
                    if partition == 'train':
                        if not LASSO or ALPHA == 0:
                            coef, residuals, rank, sv  = np.linalg.lstsq(x_train_ref.value,y_train_ref.value,rcond=-1)
                        elif LASSO:
                            clf = Lasso(alpha=ALPHA,normalize=True,max_iter=2e5)#Ridge(alpha=100.0)
                            mdl = clf.fit(x_train_ref.value, y_train_ref.value)
                            coef = clf.coef_
                    result99 = np.dot(_x,coef)
                elif method == 'mean' :
                    result99 = mean
                elif method == 'mean_custom':
                    result99 = _x.avg_deb                
                elif method == 'knn':
                    if partition == 'train':
                        knn = neighbors.KNeighborsRegressor(NB_NEIGHBORS,weights = WEIGHT,
                                                             algorithm='ball_tree',metric=METRIC,n_jobs=3)
                        knn.fit(x_train_ref.value, y_train_ref.value)
                    result99 = knn.predict(_x)
                elif method == 'mean_dow_ftime':
                    result99 = _x.mean_dow_ftime;
                if hasattr(result99,'index'):    
                    result99.index = range(0,len(result99))
                if SEPARATE_DAY_AND_RATIO:
                    if(not is_group_by_day):
                        #on avait séparé en 2 prévision celui de la moyenne de la journée et le ratio à appliquer sur cette moyenne
                        # là on recrée le y qui vaut les débit
                        data_ratio = pd.DataFrame({'ratio_debit_by_mean':result99, 'idi_jour':_x.idi_jour})
                        debit_day_and_ratio = pd.merge(data_ratio,  oldresult99[partition][method], on='idi_jour', how='left')
                        result99 = debit_day_and_ratio['ratio_debit_by_mean'] * debit_day_and_ratio['mean_debit']
                    else:
                       # if('train' == partition):
                       oldresult99[partition][method] =  pd.DataFrame({'mean_debit':result99,'idi_jour':_x.idi_jour})
                if not SEPARATE_DAY_AND_RATIO or SEPARATE_DAY_AND_RATIO and not is_group_by_day :
                       error = np.sum(np.abs( _y -  result99));
                       e[key7] += error
        total_lg += len(_y);
        
    lg_part = len(partitions)
    i_8 = 0
    tree4 = DecisionTreeRegressor(min_samples_leaf=20,max_depth=7) 
    
    #pr = tree3.predict(x_test_ref.value)
    #tree4.fit(x_test_ref.value,np.abs(pr - y_test_ref.value ))
    #from sklearn import tree
    #tree.export_graphviz(tree4, out_file='/home/borto/tree.dot',feature_names=x_train_ref.value.columns.values)
    #
    #
    for err_name,err in sorted(e.items()):
        print('erreur ' + err_name + ':' + str(err/total_lg), 'soit une explication de ' ,str((1 - err/ e['mean'+ separator +'test']) * 100), '%')
        i_8+=1    
        if i_8%lg_part == 0 : print('')
    
    tot_error += e['tree3__________test']
    tot_mean += e['mean__________test']

    
##nc = pd.DataFrame([coef],columns = x_train_ref.value.columns);
#
##importance des variables
#importances = tree6.feature_importances_
#std = np.std([tree.feature_importances_ for tree in tree6.estimators_],
#             axis=0)
#indices = np.argsort(importances)[::-1]
#



## Print the feature ranking
#print("Feature ranking:")
#
#for f in range(x_train_ref.value.shape[1]):
#    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

# Plot the feature importances of the forest
#plt.figure()
#plt.title("Feature importances")
#plt.bar(range(x_train_ref.value.shape[1]), importances[indices],
#       color="r", yerr=std[indices], align="center")
#plt.xticks(range(x_train_ref.value.shape[1]), list(x_train_ref.value.columns), rotation='vertical')
#
#plt.xlim([-1, x_train_ref.value.shape[1]])
#plt.show()











