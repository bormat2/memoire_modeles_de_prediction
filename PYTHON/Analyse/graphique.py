# débit en heure

groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in x.join(y).groupby('ftime'):
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel('Heures', fontsize=14,labelpad=20)
ax.set_ylabel('Débit', fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de l\'heure', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)


fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
plt.scatter(x.ftime.values, y, c = 'red',alpha=0.1)
ax.set_xlabel('Heures', fontsize=14,labelpad=20)
ax.set_ylabel('Débit', fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de l\'heure', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)




# debit par jour 
groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
join = x.join(y)
join = join[join['doy'] > 300 ]
for g, data in  x.join(y).groupby(['doy','year'],as_index=False)  groupby('doy'):
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel('Heures', fontsize=14,labelpad=20)
ax.set_ylabel('Débit', fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de l\'heure', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)

-------------------------

# débit en heure

groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in x.join(y).groupby('ftime'):
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel('Heures', fontsize=14,labelpad=20)
ax.set_ylabel('Débit', fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de l\'heure', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)



fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
plt.scatter(x.ftime.values, y, c = 'red',alpha=0.1)
ax.set_xlabel('Heures', fontsize=14,labelpad=20)
ax.set_ylabel('Débit', fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de l\'heure', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)





groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in x.join(y).groupby(['doy','year'],as_index=False).sum().unstack().groupby('doy'):
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel('Heures', fontsize=14,labelpad=20)
ax.set_ylabel('Débit', fontsize=14,labelpad=20)
ax.set_title('Débit par jour pour une certaine position dans l\'année', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)

#------------- par mois ---------------

ttt = pd.DataFrame({"_month":data_origin._month,"year":data_origin.year,"debit":data_origin.debit})
keys = ['year','_month']
dddd = ttt.groupby(keys,as_index=False).sum().sort_values(by=keys)
#apparemment on peut pas faire un group by sur un group by donc on on recrée une matrice
ccc = pd.DataFrame({"_month": dddd._month,"debit":dddd.debit}).groupby('_month')
groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in ccc:
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel("mois", fontsize=14,labelpad=20)
ax.set_ylabel("Débit", fontsize=14,labelpad=20)
ax.set_title('Débit par mois', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)

#------------- par jour ---------------

ttt = pd.DataFrame({"dow":data_origin.dow,"idi_jour":data_origin.idi_jour,"debit":data_origin.debit})
keys = ['idi_jour','dow']
dddd = ttt.groupby(keys,as_index=False).sum().sort_values(by=keys)
#apparemment on peut pas faire un group by sur un group by donc on on recrée une matrice
ccc = pd.DataFrame({"dow": dddd.dow,"debit":dddd.debit}).groupby('dow')
groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in ccc:
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel("jour de la semaine", fontsize=14,labelpad=20)
ax.set_ylabel("Débit", fontsize=14,labelpad=20)
ax.set_title('Débit par jour de la semaine', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)


--------------------par température--------------------------
data = data_origin[data_origin['_month'] == 4]
ttt = pd.DataFrame({'idi_jour': data.idi_jour,"temp":data.temp,"debit":data.debit})
keys = ['idi_jour']
dddd = ttt.groupby(keys,as_index=False).mean()
#apparemment on peut pas faire un group by sur un group by donc on on recrée une matrice
ccc = pd.DataFrame({"temp": pd.cut(dddd.temp - 273.15,5),"debit":dddd.debit}).groupby('temp')
groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in ccc:
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel("Température en degrès celcius", fontsize=14,labelpad=20)
ax.set_ylabel("Débit", fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de la température', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)

------------------------------vent -------------------
data = data_origin[data_origin['_month'] > 0 ]
ttt = pd.DataFrame({'idi_jour': data.idi_jour,"temp":data.rain,"debit":data.debit})
keys = ['idi_jour']
dddd = ttt.groupby(keys,as_index=False).mean()
#apparemment on peut pas faire un group by sur un group by donc on on recrée une matrice
ccc = pd.DataFrame({"temp": pd.cut(dddd.temp,5),"debit":dddd.debit}).groupby('temp')
groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in ccc:
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel("Température en degrès celcius", fontsize=14,labelpad=20)
ax.set_ylabel("Débit", fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de la température', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)

------------------------pluie

data = data_origin[data_origin['_month'] > 0 ]
ttt = pd.DataFrame({'idi_jour': data.idi_jour,"temp":data.rain,"debit":data.debit})
keys = ['idi_jour']
dddd = ttt.groupby(keys,as_index=False).mean()
#apparemment on peut pas faire un group by sur un group by donc on on recrée une matrice
ccc = pd.DataFrame({"temp": pd.cut(dddd.temp,5),"debit":dddd.debit}).groupby('temp')
groups = []
data_to_plot = []
plt.rcParams["figure.dpi"] = 75
for g, data in ccc:
    groups.append(g)
    data_to_plot.append(data.debit.values)
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data_to_plot,showmeans=True,meanline=True)
ax.set_xlabel("taux de pluie", fontsize=14,labelpad=20)
ax.set_ylabel("Débit", fontsize=14,labelpad=20)
ax.set_title('Débit en fonction de la pluie', fontsize=14)
ax.set_xticklabels( groups, ha='center', rotation=90)

--------------------nan -------------------------

data = pd.read_csv(CSV_PATH);
ttt = pd.DataFrame({"debit":data.debit, "idi_jour":data.idi_jour})
keys = ['idi_jour']
dddd = ttt.groupby(keys,as_index=False).sum()


