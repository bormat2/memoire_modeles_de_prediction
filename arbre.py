#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 24 19:26:27 2018

@author: borto
"""

import numpy as np;
import pandas as pd;
from sklearn.model_selection import train_test_split;
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage
from matplotlib import pyplot as plt
from sklearn.tree import DecisionTreeRegressor
from collections import defaultdict
from sklearn import neighbors
from sklearn import tree
from sklearn.ensemble import RandomForestRegressor
from sklearn.decomposition import PCA
from sklearn import preprocessing
import warnings
import random


class NodeBorto:
    def __init__(self, X,Y):
        self.X = X
        self.Y = Y
        self.predict = np.mean(Y)
    def mse(self):
        a = self.Y - self.predict
        return np.sum(a * a)

    def split(self):
        best_split_col_n = ''
        best_split_mse = None
        best_split_istart = 0
        self.dico = {}
        for col_n in self.X.columns:
            print(col_n)
            X = self.dico[col_n] = self.X.sort_values([col_n], ascending=True)
            x_fpart = X.iloc[0:1]
            y_fpart = self.Y.iloc[x_fpart.index]
            sum_y_fpart = np.sum(y_fpart)
            mean_f = sum_y_fpart / len(y_fpart)
            y_fpart_small = y_fpart[y_fpart['y1'] <= mean_f].sort_values('y1', ascending=True)
            y_fpart_big = y_fpart[y_fpart['y1'] > mean_f].sort_values('y1', ascending=True)

            x_lpart = X.iloc[1:]
            y_lpart = self.Y.iloc[x_lpart.index]
            sum_y_lpart = np.sum(y_lpart)
            mean_l = sum_y_lpart / len(y_lpart)
            y_lpart_small = y_lpart[y_lpart['y1'] <= mean_l].sort_values('y1', ascending=True)
            y_lpart_big = y_lpart[y_lpart['y1'] > mean_l].sort_values('y1', ascending=True)

            mae_f = abs(np.sum(y_fpart_small - mean_f)) + abs(np.sum(y_fpart_big - mean_f))
            mae_l = abs(np.sum(y_lpart_small - mean_l)) + abs(np.sum(y_lpart_big - mean_l))
            
            mae = mae_f + mae_l
        
            for i in range(1,len(self.dico[col_n])):
                line_to_move = x_lpart.tail(n)
                x_lpart.drop(line_to_move.index,inplace=True) # drop last n rows
                y_lpart_small.drop(line_to_move.index,inplace=True) # drop last n rows
                y_lpart_big.drop(line_to_move.index,inplace=True) # drop last n rows
                
#                x_fpart = X.iloc[0:i]
#                x_lpart = X.iloc[i:]
#                y_fpart = self.Y.iloc[x_fpart.index]
#                y_lpart = self.Y.iloc[x_lpart.index]
#                mean_f = np.mean(y_fpart)
#                mean_l = np.mean(y_lpart)
#                mse_f = y_fpart - mean_f
#                mse_f = np.sum(mse_f * mse_f).iloc[0] 
#                mse_l = y_lpart - mean_l
#                mse_l = np.sum(mse_l * mse_l).iloc[0] 
#
#                mse = mse_f + mse_l

                if (best_split_mse is None) or mse < best_split_mse:
                    best_split_mse = mse
                    best_split_col_n = col_n
                    best_split_istart = i
                    print('best    :    ',best_split_mse,best_split_istart)

class TreeBorto:
	 def __init__(self, X,Y):
	 	self.racine = NodeBorto(X,Y)



data = pd.DataFrame({'x1': [1,2,3,4,5], 'x2': [1,0,1,0,1] , 'y1':[9,8,7,6,5]})
data.pop()
tr = TreeBorto( data.iloc[:,0:2],data.iloc[:,2:3])
tr.racine.split()
tr.racine.X